package ch.ethz.koradir.helloasm.agents;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class HelloWorldInterceptor implements ClassFileTransformer{
    @Override
    public byte[] transform(
            ClassLoader loader,
            String className,
            Class<?> classBeingRedefined,
            ProtectionDomain protectionDomain,
            byte[] classfileBuffer
    ) throws IllegalClassFormatException {
        if(className.equals("ch/ethz/koradir/helloasm/instrumentation_targets/HelloWorld"))
            System.out.println(className + " loaded");
        return classfileBuffer;
    }
}
