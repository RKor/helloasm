package ch.ethz.koradir.helloasm.agents;

import java.lang.instrument.Instrumentation;

public class Agent {
    public static void premain(String args, Instrumentation instrumentation){
        HelloWorldInterceptor transformer = new HelloWorldInterceptor();
        instrumentation.addTransformer(transformer);
    }
}