package ch.ethz.koradir.helloasm;

import ch.ethz.koradir.helloasm.instrumentation_targets.HelloWorld;

public class Main {
    public static void main(String[] args) {
        new HelloWorld().doSomething();
    }
}