package ch.ethz.koradir.helloasm.instrumentation_targets;

import org.junit.Test;

import static org.junit.Assert.*;

public class HelloWorldTest {
    @Test
    public void doSomething() throws Exception {
        assertEquals("Hello ASM",new HelloWorld().doSomething());
    }

}